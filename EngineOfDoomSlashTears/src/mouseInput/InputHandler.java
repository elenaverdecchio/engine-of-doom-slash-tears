package mouseInput;

import gameLoop.GameLoop;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import physics.PhysicsHelperOfDoom;
import physics.PhysicsBuddyOfDoom;

public class InputHandler extends KeyAdapter {

	ArrayList<PhysicsBuddyOfDoom> physicsBuddyList;
	
	PhysicsHelperOfDoom helper;
	PhysicsBuddyOfDoom buddy1;
	PhysicsBuddyOfDoom buddy2;
	GameLoop loop;

	String[] args;
	
	private double x;
	private double y;
	
	public boolean m_keyboardStatusBuffer[];
	public boolean m_keyboardStatus[];
	public boolean m_old_keyboardStatus[];

	public InputHandler(PhysicsHelperOfDoom help, GameLoop loopy, String[] currentArgs) {
		helper = help;
		loop = loopy;
		
		args = currentArgs;
		
		int i;
		m_keyboardStatusBuffer = new boolean[KeyEvent.KEY_LAST];
		m_keyboardStatus = new boolean[KeyEvent.KEY_LAST];
		m_old_keyboardStatus = new boolean[KeyEvent.KEY_LAST];
		
		for(i=0;i<KeyEvent.KEY_LAST;i++) {
			m_keyboardStatus[i]=false;	
			m_old_keyboardStatus[i]=false;
		}
	}
	
	public void cycle() {
		for(int i=0;i<KeyEvent.KEY_LAST;i++) {
			m_old_keyboardStatus[i] = m_keyboardStatus[i];
			m_keyboardStatus[i] = m_keyboardStatusBuffer[i];
		}		
	}
	
	public void keyPressed(KeyEvent e) {
		m_keyboardStatusBuffer[e.getKeyCode()]=true;
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			physicsBuddyList = helper.getAllPhysicsBuddies();
			buddy1 = physicsBuddyList.get(0);
			buddy2 = physicsBuddyList.get(1);
			
			if (args[2].equals("server"))
			{
				Rectangle2D bob = buddy1.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				y = y - 1;
				buddy1.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy1.position = new Point2D.Double(x, y);
			}
			else if (args[2].equals("client"))
			{
				Rectangle2D bob = buddy2.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				y = y - 1;
				buddy2.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy2.position = new Point2D.Double(x, y);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			physicsBuddyList = helper.getAllPhysicsBuddies();
			buddy1 = physicsBuddyList.get(0);
			buddy2 = physicsBuddyList.get(1);
			
			if (args[2].equals("server"))
			{
				Rectangle2D bob = buddy1.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				y = y + 1;
				buddy1.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy1.position = new Point2D.Double(x, y);
			}
			else if (args[2].equals("client"))
			{
				Rectangle2D bob = buddy2.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				y = y + 1;
				buddy2.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy2.position = new Point2D.Double(x, y);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			physicsBuddyList = helper.getAllPhysicsBuddies();
			buddy1 = physicsBuddyList.get(0);
			buddy2 = physicsBuddyList.get(1);
			
			if (args[2].equals("server"))
			{
				Rectangle2D bob = buddy1.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				x = x + 1;
				buddy1.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy1.position = new Point2D.Double(x, y);
			}
			else if (args[2].equals("client"))
			{
				Rectangle2D bob = buddy2.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				x = x + 1;
				buddy2.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy2.position = new Point2D.Double(x, y);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			physicsBuddyList = helper.getAllPhysicsBuddies();
			buddy1 = physicsBuddyList.get(0);
			buddy2 = physicsBuddyList.get(1);
			
			if (args[2].equals("server"))
			{
				Rectangle2D bob = buddy1.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				x = x - 1;
				buddy1.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy1.position = new Point2D.Double(x, y);
			}
			else if (args[2].equals("client"))
			{
				Rectangle2D bob = buddy2.shape.getFrame();
				x = bob.getX();
				y = bob.getY();
				x = x - 1;
				buddy2.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy2.position = new Point2D.Double(x, y);
			}
			
//			physicsBuddyList = helper.getAllPhysicsBuddies();
//			buddy = helper.getPhysicsBuddyOfDoom();
//
//			Rectangle2D bob = buddy.shape.getFrame();
//			x = bob.getX();
//			y = bob.getY();
//			x = x - 1;
//			buddy.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));
		}
	}
	
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		m_keyboardStatusBuffer[e.getKeyCode()]=false;
	}

}

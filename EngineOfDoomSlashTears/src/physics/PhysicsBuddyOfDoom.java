package physics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;


public class PhysicsBuddyOfDoom {

	//Buddy Variables
	public Ellipse2D shape;
	public Point2D position;
	public Point2D previousPosition;
	public int buddyType; //0 is 1st player, 1 is 2nd player, 2 is enemy
	public float mass;
	public float linearVelocity;
	public float linearAcceleration;
	public float angularVelocity;
	public float angularAcceleration;

	public PhysicsBuddyOfDoom() {
		shape = new Ellipse2D.Float(30.0f, 30.0f, 30.0f, 30.0f);
		position = new Point2D.Float(30, 30);
		previousPosition = new Point2D.Float(30, 30);
		mass = 10;
		linearVelocity = 1;
		linearAcceleration = 1;
		angularVelocity = 0;
		angularAcceleration = 0;
		buddyType = 0;
	}

	public PhysicsBuddyOfDoom(Ellipse2D newShape, Point2D newPosition, int newType) {
		shape = newShape;
		position = newPosition;
		previousPosition = newPosition;
		mass = 10;
		linearVelocity = 1;
		linearAcceleration = 1;
		angularVelocity = 0;
		angularAcceleration = 0;
		buddyType = newType;
	}

	public PhysicsBuddyOfDoom(Ellipse2D newShape, Point newPosition, Point newPreviousPosition, float newMass, float newLinearVelocity, float newLinearAcceleration, float newAngularVelocity, float newAngularAcceleration) {
		shape = newShape;
		position = newPosition;
		previousPosition = newPreviousPosition;
		mass = newMass;
		linearVelocity = newLinearVelocity;
		linearAcceleration = newLinearAcceleration;
		angularVelocity = newAngularVelocity;
		angularAcceleration = newAngularAcceleration;
	}

	public void setBuddyShape(Ellipse2D newShape) {
		shape = newShape;
	}

	public Ellipse2D getBuddyShape() {
		return shape;
	}

	public void setBuddyPosition(Point newPosition) {
		position = newPosition;
	}

	public Point2D getBuddyPosition() {
		return position;
	}
	
	public void setBuddyPreviousPosition(Point newPreviousPosition) {
		previousPosition = newPreviousPosition;
	}

	public Point2D getBuddyPreviousPosition() {
		return previousPosition;
	}

	public void setBuddyMass(float newMass) {
		mass = newMass;
	}

	public float getBuddyMass() {
		return mass;
	}

	public void setBuddyLinearVelocity(float newLinearVelocity) {
		linearVelocity = newLinearVelocity;
	}

	public float getBuddyLinearVelocity() {
		return linearVelocity;
	}

	public void setBuddyLinearAcceleration(float newLinearAcceleration) {
		linearAcceleration = newLinearAcceleration;
	}

	public float getBuddyLinearAcceleration() {
		return linearAcceleration;
	}

	public void setBuddyAngularVelocity(float newAngularVelocity) {
		angularVelocity = newAngularVelocity;
	}

	public float getBuddyAngularVelocity() {
		return angularVelocity;
	}

	public void setBuddyAngularAcceleration(float newAngularAcceleration) {
		angularAcceleration = newAngularAcceleration;
	}

	public float getBuddyAngularAcceleration() {
		return angularAcceleration;
	}

	public void paintComponent(Graphics g) {

		Graphics2D ga = (Graphics2D)g;
		ga.setPaint(Color.BLACK);
		ga.draw(shape);
		switch (buddyType) {
		case 0:
			ga.setPaint(Color.BLUE);
			break;
		case 1:
			ga.setPaint(Color.GREEN);
			break;
		case 2:
			ga.setPaint(Color.RED);
			break;
		default:
			ga.setPaint(Color.PINK);
			break;
			
		}
		ga.fill(shape);
	}

	public Ellipse2D getShapeToPaint() {
		
		return shape;
	}
}

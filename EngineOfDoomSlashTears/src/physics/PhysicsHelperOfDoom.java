package physics;

import java.awt.Dimension;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import physics.PhysicsBuddyOfDoom;

public class PhysicsHelperOfDoom {
	
	public ArrayList<PhysicsBuddyOfDoom> physicsBuddyList;

	//Vertlet Variables
	private double x;
	private double y;
	private double gravity;
	
	private double collisionX;
	private double collisionY;
	
	private double newX1;
	private double newX2;
	private double newY1;
	private double newY2;
	
	public PhysicsHelperOfDoom(){
		physicsBuddyList = new ArrayList<PhysicsBuddyOfDoom>();
		
		x = 30;
		y = 30;
		gravity = -90;
		
		collisionX = 0;
		collisionY = 0;
		
		newX1 = 0;
		newX2 = 0;
		newY1 = 0;
		newY2 = 0;
	}

	public void createBuddyOfDoom() {
		
		PhysicsBuddyOfDoom newBuddy = new PhysicsBuddyOfDoom();
		physicsBuddyList.add(newBuddy);
	} 

	public void createBuddyOfDoom(Ellipse2D shape, Point2D position, int type) {
		
		PhysicsBuddyOfDoom newBuddy = new PhysicsBuddyOfDoom(shape, position, type);
		physicsBuddyList.add(newBuddy);
	} 
	
	public PhysicsBuddyOfDoom getPhysicsBuddyOfDoom() {
		
		//System.out.println("Current number of buddies: " + physicsBuddyList.size());
		PhysicsBuddyOfDoom currentBuddy = physicsBuddyList.get(0);
		
		return currentBuddy;
	}
	
	public ArrayList<PhysicsBuddyOfDoom> getAllPhysicsBuddies() {
		//System.out.println("Current number of buddies: " + physicsBuddyList.size());
		return physicsBuddyList;
	}
	
	public PhysicsBuddyOfDoom getEnemyBuddy() {
		for (PhysicsBuddyOfDoom buddy : physicsBuddyList) {
			if (buddy.buddyType == 2)
				return buddy;
		}
		return null;
	}
	
	public PhysicsBuddyOfDoom getPlayer1Buddy() {
		for (PhysicsBuddyOfDoom buddy : physicsBuddyList) {
			if (buddy.buddyType == 0)
				return buddy;
		}
		return null;
	}
	
	public PhysicsBuddyOfDoom getPlayer2Buddy() {
		for (PhysicsBuddyOfDoom buddy : physicsBuddyList) {
			if (buddy.buddyType == 1)
				return buddy;
		}
		return null;
	}
	
	public void updateVertletIntegration() {
		for (PhysicsBuddyOfDoom buddy : physicsBuddyList) {
			if (buddy.buddyType == 2)
				break;
			Rectangle2D bob = buddy.shape.getFrame();

			x = bob.getX();
			y = bob.getY();

			double tempPointX = bob.getX();
			double tempPointY = bob.getY();
			x = x + (x - buddy.previousPosition.getX());
			y = y + (y - buddy.previousPosition.getY() - gravity*(0.05*0.05)); //Gravity would be added here!
			buddy.shape.setFrame(new Rectangle2D.Float((float)x, (float)y, (float)bob.getWidth(), (float)bob.getHeight()));

			buddy.previousPosition = new Point2D.Double(tempPointX, tempPointY);
			buddy.position = new Point2D.Double(x, y);
		}
	}

	public void checkBuddyCollisions() {
		for (PhysicsBuddyOfDoom buddyFirstLoopBuddyOfDoom : physicsBuddyList) {
			for (PhysicsBuddyOfDoom buddySecondLoopBuddyOfDoom : physicsBuddyList) {
				if (buddyFirstLoopBuddyOfDoom != buddySecondLoopBuddyOfDoom) {
					Rectangle2D bob1 = buddyFirstLoopBuddyOfDoom.shape.getFrame();
					Rectangle2D bob2 = buddySecondLoopBuddyOfDoom.shape.getFrame();
					
					if (bob1.intersects(bob2)) {
						newX1 = bob1.getX() - 10;
						newY1 = bob1.getY() - 10;
						newX2 = bob2.getX() + 10;
						newY2 = bob2.getY() + 10;
						buddyFirstLoopBuddyOfDoom.shape.setFrame(new Rectangle2D.Float((float)newX1, (float)newY1, (float)bob1.getWidth(), (float)bob1.getHeight()));
						buddySecondLoopBuddyOfDoom.shape.setFrame(new Rectangle2D.Float((float)newX2, (float)newY2, (float)bob2.getWidth(), (float)bob2.getHeight()));
						
						buddyFirstLoopBuddyOfDoom.position = new Point2D.Double(newX1, newY1);
						buddySecondLoopBuddyOfDoom.position = new Point2D.Double(newX2, newY2);
					}
					
				}
			}
		}


		
		
	}
	public void checkWorldCollision(Dimension worldDimensions) {
		for (PhysicsBuddyOfDoom buddy : physicsBuddyList) {
			Rectangle2D bob = buddy.shape.getFrame();

			collisionX = bob.getX();
			collisionY = bob.getY();

			if (collisionX + bob.getWidth() > worldDimensions.width) {
				buddy.shape.setFrame(new Rectangle2D.Float((float)collisionX-10, (float)collisionY, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy.position = new Point2D.Double(x-10, y);
			} else if (collisionY + (2*bob.getHeight()) > worldDimensions.height) {
				buddy.shape.setFrame(new Rectangle2D.Float((float)collisionX, (float)collisionY-10, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy.position = new Point2D.Double(x, y-10);
			} else if (collisionX < 0) {
				buddy.shape.setFrame(new Rectangle2D.Float((float)collisionX+10, (float)collisionY, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy.position = new Point2D.Double(x+10, y);
			} else if (collisionY < 0) {
				buddy.shape.setFrame(new Rectangle2D.Float((float)collisionX, (float)collisionY+10, (float)bob.getWidth(), (float)bob.getHeight()));
				buddy.position = new Point2D.Double(x, y+10);
			}
		}
	}
}

package networking;


import java.net.*;
import java.io.*;
import java.io.PrintWriter;


public class Client {
	
	   public void connect(String serverName, int port)
	   {
	      try
	      {
	         System.out.println("Connecting to " + serverName
	                             + " on port " + port);
	         Socket client = new Socket(serverName, port);
	         System.out.println("Just connected to "
	                      + client.getRemoteSocketAddress());
	         //OutputStream outToServer = client.getOutputStream();
	         //DataOutputStream out = new DataOutputStream(outToServer);
	       PrintWriter out = new PrintWriter(client.getOutputStream(), true);
	       BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));      
	       
	       out.print("client test");
	       String line = in.readLine();
	       System.out.println(line);
	    
	      }catch(IOException e)
	      {
	         e.printStackTrace();
	      }
	   }
	
	public void retrieveUpdates() {
		// sync with server

		System.out.println("Client synced with server.");
	}

}

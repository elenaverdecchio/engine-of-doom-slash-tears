package networking;

import java.net.*;
import java.io.*;

import com.sun.tracing.dtrace.ArgsAttributes;
import java.io.BufferedReader;

public class Server extends Thread {

	private ServerSocket serverSocket;
	
	public Server(int port) throws IOException
	   {
	      serverSocket = new ServerSocket(port);
	      //serverSocket.setSoTimeout(10000);
	   }
	
	 public void run()
	   {
	      while(true)
	      {
	         try
	         {
	            System.out.println("Waiting for client on port " +
	            serverSocket.getLocalPort() + "...");
	            Socket server = serverSocket.accept();
	            System.out.println("Just connected from " + server.getRemoteSocketAddress());
	            //DataInputStream in = new DataInputStream(server.getInputStream());
	            BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
	            PrintWriter out = new PrintWriter(server.getOutputStream(), true);
	            
	            out.println("server test");
	            String line = in.readLine();
	            System.out.println(line);
	         
	         }catch(SocketTimeoutException s)
	         {
	            System.out.println("Socket timed out!");
	            break;
	         }catch(IOException e)
	         {
	            e.printStackTrace();
	            break;
	         }
	      }
	   }
	 
	public void startServer(int port){
	      try
	      {
	         Thread t = new Server(port);
	         t.start();
	      }catch(IOException e)
	      {
	         e.printStackTrace();
	      }
	}
	
	public void postUpdates() {
		// post updates for client
		System.out.println("Server updates posted.");
	}

}

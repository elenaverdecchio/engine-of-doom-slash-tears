package Engine;


import java.io.IOException;

import artificialIntelligence.AIManager;
import collisionDetection.CollisionDetector;
import physics.PhysicsHelperOfDoom;
import physics.PhysicsUpdater;
import resourceManagement.SpriteManager;
import networking.Client;
import networking.Server;

public class GameEngine {
	private Client client;
	private Server server;
	private PhysicsUpdater physicsUpdater;
	private SpriteManager spriteManager;
	private CollisionDetector collisionDetector;
	private AIManager ai;

	public int port;
	
	public GameEngine(PhysicsHelperOfDoom helper) throws IOException {
		client = new Client();
		server = new Server(0);
		physicsUpdater = new PhysicsUpdater();
		spriteManager = new SpriteManager();
		collisionDetector = new CollisionDetector();
		ai = new AIManager(helper);
	}

	public void pollForOSMessages() {
		
	}

	public void updateAI() {
		ai.update();
	}

	public void updatePhysics() {
		collisionDetector.checkCollisions();
		physicsUpdater.update();
	}

	public void updateStatistics() {
		// pull from "State" class?
	}

	public void render() {
		// graphics rendering
	}

	public void postServerUpdates() {
		// server-side - post updates
		server.postUpdates();
	}
	
	public void startServer(String[] args){
		// server-side - starts new server
		int port = Integer.parseInt(args[1]);
		server.startServer(port);
	}

	public void retrieveClientUpdates() {
		// client-side - retrieveUpdates
		client.retrieveUpdates();
	}
	
	public void clientConnect(String[] args){
		String serverName = args[0];
	    int port = Integer.parseInt(args[1]);
	    client.connect(serverName, port);
	}

	public void loadGame() {
		// load objects of game, will have parameters of game stuff
		spriteManager.load();
	}

	
}

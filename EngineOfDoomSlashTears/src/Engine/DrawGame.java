package Engine;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import physics.PhysicsBuddyOfDoom;
import physics.PhysicsHelperOfDoom;

public class DrawGame{

	PhysicsHelperOfDoom helper;
	PhysicsBuddyOfDoom buddy;
	ArrayList<PhysicsBuddyOfDoom> buddies;

	public DrawGame(PhysicsHelperOfDoom helperr) {
		helper = helperr;
		buddy = helper.getPhysicsBuddyOfDoom();
		buddies = helper.getAllPhysicsBuddies();
	}
	
	public void init(PhysicsHelperOfDoom helperr)
	{
		helper = helperr;
		buddy = helper.getPhysicsBuddyOfDoom();
		buddies = helper.getAllPhysicsBuddies();
	}

	public void update(Graphics g)  
	{  
		paint(g);  
	}  

	public void paint(Graphics g)  
	{  
		Graphics2D ga = (Graphics2D)g;
		Rectangle2D background = new Rectangle2D.Float(0, 0, 600, 600);
		ga.draw(background);
		ga.setPaint(Color.BLACK);
		ga.fill(background);
		
		buddy = helper.getPhysicsBuddyOfDoom();
		buddies = helper.getAllPhysicsBuddies();
		for (PhysicsBuddyOfDoom buddyOfDoom : buddies) {
			buddyOfDoom.paintComponent(g);
		}
		
//		buddy.paintComponent(g);   
	} 


}

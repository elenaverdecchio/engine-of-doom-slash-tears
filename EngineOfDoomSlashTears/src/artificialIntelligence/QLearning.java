package artificialIntelligence;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import physics.PhysicsBuddyOfDoom;
import physics.PhysicsHelperOfDoom;

public class QLearning {

	private QState[] myStates;
	private QAction[] myActions;
	private int[][] myR;
	private double[][] myQ;
	final double gamma = 0.9;
	final double alpha = 0.1;
	private QState currentState;
	PhysicsHelperOfDoom helper;



	public QLearning(QState[] states, QAction[] actions, int[][] r, double[][] q, PhysicsHelperOfDoom helperr) {
		myStates = states;
		myActions = actions;
		myR = r;
		myQ = q;
		helper = helperr;

//		Random rand = new Random();
//		currentState = myStates[rand.nextInt(myStates.length)];
		
		currentState = getState(helper.getEnemyBuddy());
	}
	
	public QState getState(PhysicsBuddyOfDoom buddy) {
		double xDoub = buddy.shape.getCenterX();
		double yDoub = buddy.shape.getCenterY();
		int x = (int) Math.floor(xDoub);
		int y = (int) Math.floor(yDoub);
		int state = ((y/100)*6)+(x/100);
		return myStates[state];
	}
	
	public void setState(PhysicsBuddyOfDoom buddy, QState nextState) {

		Rectangle2D bob = buddy.shape.getFrame();

		double x = (nextState.stateId % 6)*100 + 25;
		double y = (nextState.stateId / 6)*100 + 25;
		buddy.shape.setFrame(x ,y ,bob.getWidth(), bob.getHeight());
		buddy.position = new Point2D.Double(x,y);
	}


	public void updateLearning() {

		Random rand = new Random();

		// select a (random) action from possible actions
		QAction actionsFromState = myActions[currentState.stateId];
		// below needs to change, don't want to pick randomly
		int index = rand.nextInt(actionsFromState.transitionStates.length);
		QState nextState = actionsFromState.transitionStates[index];

//		double maxR = Double.MIN_VALUE; // if one state has highest R, choose that, if not, keep random from above
//		for (int i = 0; i < actionsFromState.transitionStates.length; i++) {
//			if (Q(currentState, actionsFromState.transitionStates[i]) > maxR) {
//				maxR = Q(currentState, actionsFromState.transitionStates[i]);
//				nextState = actionsFromState.transitionStates[i];
//			}
//		}
		nextState = maxQState(currentState);

		// consider next state using this action
		double q = Q(currentState, nextState);
		double maxQ = maxQ(nextState);
		int r = R(currentState, nextState);

		double value = q + alpha * (r + gamma * maxQ - q);
		setQ(currentState, nextState, value);
		// Set the next state as the current state
		currentState = nextState;
		setState(helper.getEnemyBuddy(), nextState);
		
	}

	double maxQ(QState s) {
		QAction actionFromState = myActions[s.stateId];
		QState[] possibleNextStates = actionFromState.transitionStates;
		double maxValue = Double.MIN_VALUE;
		for (int i = 0; i < possibleNextStates.length; i++) {
			QState nextState = possibleNextStates[i];
			double origValue = myQ[s.stateId][nextState.stateId] + 400;
			double valueForDistanceFromPlayer = getDistance(possibleNextStates[i], helper.getPlayer1Buddy());
			
			double value = origValue / valueForDistanceFromPlayer;
			
			if (value > maxValue)
				maxValue = value;
		}
		return maxValue;
	}

	QState maxQState(QState s) {
		QAction actionFromState = myActions[s.stateId];
		QState[] possibleNextStates = actionFromState.transitionStates;
		QState bestState = possibleNextStates[0];
		double maxValue = Double.MIN_VALUE;
		for (int i = 0; i < possibleNextStates.length; i++) {
			QState nextState = possibleNextStates[i];
			double origValue = myQ[s.stateId][nextState.stateId] + 400;
			double valueForDistanceFromPlayerOne = getDistance(possibleNextStates[i], helper.getPlayer1Buddy());
			double value = origValue / valueForDistanceFromPlayerOne;
			double valueForDistanceFromPlayerTwo = getDistance(possibleNextStates[i], helper.getPlayer2Buddy());
			if (value < (origValue / valueForDistanceFromPlayerTwo))
				value = origValue / valueForDistanceFromPlayerTwo;
			
			if (value > maxValue) {
				maxValue = value;
				bestState = nextState;
			}
		}
		return bestState;
	}
	
	public double getDistance(QState nextState, PhysicsBuddyOfDoom player) {
		double distance = 0;

		double xSquared = (((nextState.stateId % 6)*100 + 25)-player.shape.getCenterX())*(((nextState.stateId % 6)*100 + 25)-player.shape.getCenterX());
		double ySquared = (((nextState.stateId / 6)*100 + 25)-player.shape.getCenterY())*(((nextState.stateId / 6)*100 + 25)-player.shape.getCenterY());
		double cSquared = xSquared + ySquared;
		distance = Math.sqrt(cSquared);
		return distance;
	}
	

	public void runLearning() {
		Random rand = new Random();
		System.out.println("Starting Learning");
		for (int i = 0; i < 1000; i++) {
			// Select random initial state
			QState state = myStates[rand.nextInt(myStates.length)];

			// loop until reached goal state
			while (state != myStates[2]) {

				// select a (random) action from possible actions
				QAction actionsFromState = myActions[state.stateId];
				int index = rand.nextInt(actionsFromState.transitionStates.length);
				QState nextState = actionsFromState.transitionStates[index];

				// consider next state using this action
				double q = Q(state, nextState);
				double maxQ = maxQ(nextState);
				int r = R(state, nextState);

				double value = q + alpha * (r + gamma * maxQ - q);
				setQ(state, nextState, value);

				// Set the next state as the current state
				state = nextState;
			}
		}

		System.out.println("Ended Learning");
	}

	// get policy from state
	QState policy(QState state) {
		QAction actionFromState = myActions[state.stateId];
		QState[] possibleNextStates = actionFromState.transitionStates;
		double maxValue = Double.MIN_VALUE;
		QState policyGotoState = state; // default goto self if not found
		for (int i = 0; i < possibleNextStates.length; i++) {
			QState nextState = possibleNextStates[i];
			double value = myQ[state.stateId][nextState.stateId];

			if (value > maxValue) {
				maxValue = value;
				policyGotoState = nextState;
			}
		}
		return policyGotoState;
	}

	double Q(QState s, QState a) {
		return myQ[s.stateId][a.stateId];
	}

	void setQ(QState s, QState a, double value) {
		myQ[s.stateId][a.stateId] = value;
	}

	int R(QState s, QState a) {
		return myR[s.stateId][a.stateId];
	}

	void printResult() {
		System.out.println("Print result");
		for (int i = 0; i < myQ.length; i++) {
			System.out.print("out from " + myStates[i].stateId + ":  ");
			for (int j = 0; j < myQ[i].length; j++) {
				System.out.print("" + (myQ[i][j]) + " ");
			}
			System.out.println();
		}
	}

	// policy is maxQ(states)
	void showPolicy() {
		System.out.println("\nshowPolicy");
		for (int i = 0; i < myStates.length; i++) {
			QState from = myStates[i];
			QState to =  policy(from);
			System.out.println("from "+myStates[from.stateId].stateId+" goto "+myStates[to.stateId].stateId);
		}   
	}

	public void printCurrentState() {

		System.out.println("Current State = " + currentState.stateId);
	}

}

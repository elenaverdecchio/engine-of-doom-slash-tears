package artificialIntelligence;

public class QAction {
	
	QState currentState;
	QState[] transitionStates;
	
	public QAction(QState current, QState[]transition) {
		currentState = current;
		transitionStates = transition;
	}
}

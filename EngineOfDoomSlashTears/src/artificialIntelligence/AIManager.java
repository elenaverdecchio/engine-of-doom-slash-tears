package artificialIntelligence;

import physics.PhysicsHelperOfDoom;


public class AIManager {
	QLearning qLearning;
	QState[] states;
	QAction[] actions;
	int[][] R;
	double[][] Q;

	public static void main(String[] args) {
	}

	public AIManager(PhysicsHelperOfDoom helper) {
		states = new QState[36];
		// Create all states, state array, and state count
		for (int i = 0; i < 36; i++) {
			QState state = new QState(i);
			state.stateId = i;
			states[i] = state;
		}
//		QState zeroState = new QState(0);
//		QState oneState = new QState(1);
//		QState twoState = new QState(2);
//		QState threeState = new QState(3);
//		QState fourState = new QState(4);
//		QState fiveState = new QState(5);
//		states = new QState[]{zeroState, oneState, twoState, threeState, fourState, fiveState};
		int stateCount = states.length;

		// Create all actions with current state and array of transitionable states
		actions = new QAction[36];
		int[] topLeft = new int[]{0};
		int[] top = new int[]{1, 2, 3, 4};
		int[] topRight = new int[]{5};
		
		int[] left = new int[]{6, 12, 18, 24};
		int[] middle = new int[]{7, 8, 9, 10, 13, 14, 15, 16, 19, 20, 21, 22, 25, 26, 27, 28};
		int[] right = new int[]{11, 17, 23, 29};
		
		int[] bottomLeft = new int[]{30};
		int[] bottom = new int[]{31, 32, 33, 34};
		int[] bottomRight = new int[]{35};
		
		for (int i : topLeft) {
			QAction action = new QAction(states[i], new QState[]{states[i+1], states[i+6], states[i+7]});
			actions[i]= action; 
		}
		
		for (int i : top) {
			QAction action = new QAction(states[i], new QState[]{states[i-1], states[i+1], states[i+5], states[i+6], states[i+7]});
			actions[i]= action; 
		}
		
		for (int i : topRight) {
			QAction action = new QAction(states[i], new QState[]{states[i-1], states[i+5], states[i+6]});
			actions[i]= action; 
		}
		
		for (int i : left) {
			QAction action = new QAction(states[i], new QState[]{states[i-6], states[i-5], states[i+1], states[i+6], states[i+7]});
			actions[i]= action; 
		}
		
		for (int i : middle) {
			QAction action = new QAction(states[i], new QState[]{states[i-7], states[i-6], states[i-5], states[i-1], states[i+1], states[i+5], states[i+6], states[i+7]});
			actions[i]= action; 
		}
		
		for (int i : right) {
			QAction action = new QAction(states[i], new QState[]{states[i-7], states[i-6], states[i-1], states[i+5], states[i+6]});
			actions[i]= action; 
		}
		
		for (int i : bottomLeft) {
			QAction action = new QAction(states[i], new QState[]{states[i-6], states[i-5], states[i+1]});
			actions[i]= action; 
		}
		
		for (int i : bottom) {
			QAction action = new QAction(states[i], new QState[]{states[i-7], states[i-6], states[i-5], states[i-1], states[i+1]});
			actions[i]= action; 
		}
		
		for (int i : bottomRight) {
			QAction action = new QAction(states[i], new QState[]{states[i-7], states[i-6], states[i-1]});
			actions[i]= action; 
		}
//		QAction zeroActions = new QAction(zeroState, new QState[]{oneState, threeState});
//		QAction oneActions = new QAction(oneState, new QState[]{zeroState, twoState, fourState});
//		QAction twoActions = new QAction(twoState, new QState[]{twoState});
//		QAction threeActions = new QAction(threeState, new QState[]{zeroState, fourState});
//		QAction fourActions = new QAction(fourState, new QState[]{oneState, threeState, fiveState});
//		QAction fiveActions = new QAction(fiveState, new QState[]{twoState, fourState});
//		actions = new QAction[]{zeroActions, oneActions, twoActions, threeActions, fourActions, fiveActions};

		R = new int[stateCount][stateCount]; // for rewards
		Q = new double[stateCount][stateCount]; // for Q learning

		// Set initial values, goal is state 2, so transitions to 2 are 100
//		R[oneState.stateId][twoState.stateId] = 100;
//		R[fiveState.stateId][twoState.stateId] = 100;

		qLearning = new QLearning(states, actions, R, Q, helper);
	}

	public void runAI() {
		qLearning.runLearning();
       // qLearning.printResult();
       // qLearning.showPolicy();

	}

	public void update() {
		qLearning.updateLearning();
//		qLearning.printCurrentState();
        //qLearning.showPolicy();
	}


}

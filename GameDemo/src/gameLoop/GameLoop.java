package gameLoop;


import gameLoader.GameLoader;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;

//import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.sun.tools.corba.se.idl.Arguments;

import mouseInput.InputHandler;

import physics.PhysicsBuddyOfDoom;
import physics.PhysicsHelperOfDoom;
import sun.misc.Timeable;
import Engine.DrawGame;
import Engine.GameEngine;


public class GameLoop extends JFrame implements Timeable{
	//extend JFrame
	//call repaint
	//implement paint
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	static DrawGame render;
	static InputHandler mousey;
	PhysicsHelperOfDoom helper;
	static JFrame frame;
	static DrawPane pane;
	static int count;
	static String[] currentArgs;
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		currentArgs = args;
		GameLoop me = new GameLoop();
		
		frame = new JFrame("Game Engine");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(600, 600));
		pane = new DrawPane(me.getDrawGame(), me.getInputHandler());
		frame.add(pane);
		frame.addKeyListener(mousey);
		frame.pack();
		frame.setVisible(true);
		
		me.run(args);
	}

	public GameLoop() {

		helper = new PhysicsHelperOfDoom();
		Ellipse2D playerShape = new Ellipse2D.Float(30.0f, 30.0f, 30.0f, 30.0f);
		Point2D playerPosition = new Point2D.Float(30, 30);
		helper.createBuddyOfDoom(playerShape, playerPosition, 0);
		
		Ellipse2D playerTwoShape = new Ellipse2D.Float(30.0f, 160.0f, 30.0f, 30.0f);
		Point2D playerTwoPosition = new Point2D.Float(30, 160);
		helper.createBuddyOfDoom(playerTwoShape, playerTwoPosition, 1);

		Ellipse2D enemyShape = new Ellipse2D.Float(160.0f, 30.0f, 50.0f, 50.0f);
		Point2D enemyPosition = new Point2D.Float(160, 30);
		helper.createBuddyOfDoom(enemyShape, enemyPosition, 2);
		count = 0;
		
		mousey = new InputHandler(helper, this, currentArgs);
//		addMouseListener(mousey);
//		addMouseMotionListener(mousey);

		render = new DrawGame(helper);
	}
	
	public DrawGame getDrawGame() {
		
		return render;
	}
	
	public InputHandler getInputHandler() {
		
		return mousey;
	}

	private void run(String[] args) {
		GameEngine engine;
		try {
			engine = new GameEngine(helper);
		} catch (IOException e) {
			engine = null;
			e.printStackTrace();
		} 
		GameLoader loader = new GameLoader();
		loader.loadGame(engine);
		//GameTimer timer = new GameTimer();
		boolean quit = true;
		
		int state = 0;
		
		engine.render();

		long next_frame_time = System.currentTimeMillis();
		int T = 50; // 50 is 1000 / 20 (so 20 frames per second)
		
		if (args[2].equals("server"))
		{
			state = 2;
			engine.startServer(args);
		}
		else if (args[2].equals("client"))
		{
			state = 3;
			engine.clientConnect(args);
		}
		
		while(quit) { 
			long current_time = System.currentTimeMillis();
			
			if (!(current_time >= next_frame_time)) {
				try {
					Thread.sleep(1);
				} catch (Exception e) {					
				}
				continue;
			}
			
			next_frame_time += T;
			helper.checkWorldCollision(new Dimension(600, 600));
			helper.checkBuddyCollisions();
			helper.updateVertletIntegration();

			pane.repaint();
			engine.pollForOSMessages(); 
			if (count % 30 == 0)
				engine.updateAI();  
			if (true) // as often as possible
				//engine.updatePhysics(); 
			engine.updateStatistics(); // Get information from the game engine to update the player score, health, etc. (since typically the HUD is not rendered by the game engine, but separately)
			//			if (timer.value%33 == 0) // ~30 fps (need to figure out)

			//			FPScontrol(); 
			count++;
			
		} 

		// game has quit
		engine = null;
	}

	public void tick(sun.misc.Timer arg0) {

	}


}

class DrawPane extends JPanel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	DrawGame render;
	InputHandler mousey;
	
	public DrawPane(DrawGame drawer, InputHandler handler) {
		render = drawer;
		
		mousey = handler;
		addKeyListener(mousey);
//		addMouseListener(mousey);
//		addMouseMotionListener(mousey);
	}
	
	public void update(Graphics g)  
	{  
		paint(g);
	}  

	public void paint(Graphics g)  
	{  
		render.paint(g);
	} 
 }

package gameLoop;

import sun.misc.Timeable;
import sun.misc.Timer;


public class GameTimer implements Timeable {
	Timer timer;
	int value;
	
	public GameTimer() {
		timer = new Timer(this, 1);
		value = 0;
		timer.cont();
	}
	
	public void tick(Timer arg0) {
		value++;
	}

}

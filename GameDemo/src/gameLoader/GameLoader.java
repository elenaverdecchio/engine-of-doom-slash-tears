package gameLoader;

import Engine.GameEngine;

public class GameLoader {

	public void loadGame(GameEngine engine) {
		// loads game, will include sprites / levels / characters to be loaded, etc
		System.out.println("Loading Game...");
		engine.loadGame();
	}

}
